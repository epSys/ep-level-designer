﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml;

namespace EPDesigner
{
	internal static class LevelData
	{
		public static string Save(Canvas canvas, string name)
		{
			var sb = new StringBuilder();
			sb.AppendLine(string.Format(
				"<DOMSymbolItem xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://ns.adobe.com/xfl/2008/\" name=\"Levels/{0}\" linkageExportForAS=\"true\" linkageClassName=\"{0}\" sourceLibraryItemHRef=\"Levels/{0}\">",
				name));
			sb.AppendLine("  <timeline>");
			sb.AppendLine(string.Format("    <DOMTimeline name=\"{0}\">", name));
			sb.AppendLine("      <layers>");
			sb.AppendLine("        <DOMLayer name=\"Z\">");
			sb.AppendLine("          <frames>");
			sb.AppendLine("            <DOMFrame index=\"0\">");
			sb.AppendLine("              <elements>");
			foreach (var i in canvas.Children.OfType<Tile>())
			{
				string id = i.Descriptor.XmlID;
				string n = i.TileName;
				sb.AppendLine(string.Format(
					"                <DOMSymbolInstance libraryItemName=\"{0}\" name=\"{1}\">", id, n));

				var matrix = new Matrix();
				var xScale = i.ActualWidth / i.Descriptor.Size.Width;
				var yScale = i.ActualHeight / i.Descriptor.Size.Height;
				matrix.ScaleAt(
					xScale,
					yScale,
					i.ActualWidth * xScale / 2,
					i.ActualWidth * yScale / 2);
				matrix.OffsetX = matrix.OffsetY = 0;
				matrix.RotateAt(i.Rotation,
					i.ActualWidth * xScale / 2,
					i.ActualHeight * yScale / 2);
				matrix.Translate(
					Canvas.GetLeft(i) + i.Descriptor.OffsetPoint.X * xScale,
					Canvas.GetTop(i) + i.Descriptor.OffsetPoint.Y * yScale);

				sb.AppendLine(
					"                    <matrix>");
				sb.AppendLine(string.Format(
					"                        <Matrix a=\"{0}\" b=\"{1}\" c=\"{2}\" d=\"{3}\" tx=\"{4}\" ty=\"{5}\" />", matrix.M11,
					matrix.M12, matrix.M21, matrix.M22, matrix.OffsetX, matrix.OffsetY));
				sb.AppendLine(
					"                    </matrix>");

				sb.AppendLine(
					"                </DOMSymbolInstance>");
			}
			sb.AppendLine("              </elements>");
			sb.AppendLine("            </DOMFrame>");
			sb.AppendLine("          </frames>");
			sb.AppendLine("        </DOMLayer>");
			sb.AppendLine("      </layers>");
			sb.AppendLine("    </DOMTimeline>");
			sb.AppendLine("  </timeline>");
			sb.AppendLine("</DOMSymbolItem>");
			return sb.ToString();
		}

		public static void Load(Canvas canvas, string xml)
		{
			var xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml);
			var mgr = new XmlNamespaceManager(xmlDoc.NameTable);
			mgr.AddNamespace("x", "http://ns.adobe.com/xfl/2008/");

			canvas.Children.Clear();
			foreach (
				XmlNode i in
					xmlDoc.SelectNodes(
						"/x:DOMSymbolItem/x:timeline/x:DOMTimeline/x:layers/x:DOMLayer/x:frames/x:DOMFrame/x:elements/x:DOMSymbolInstance",
						mgr))
			{
				string id = i.Attributes["libraryItemName"].Value;
				TileDesc desc;
				if (!TileDesc.XmlTiles.TryGetValue(id, out desc)) continue;
				Tile t = desc.CreateTile();
				t.TileName = i.Attributes["name"].Value;

				XmlNode m = i.SelectSingleNode("x:matrix/x:Matrix", mgr);
				var matrix = new Matrix();
				if (m != null)
				{
					if (m.Attributes["a"] != null)
						matrix.M11 = double.Parse(m.Attributes["a"].Value);
					if (m.Attributes["b"] != null)
						matrix.M12 = double.Parse(m.Attributes["b"].Value);
					if (m.Attributes["c"] != null)
						matrix.M21 = double.Parse(m.Attributes["c"].Value);
					if (m.Attributes["d"] != null)
						matrix.M22 = double.Parse(m.Attributes["d"].Value);
					if (m.Attributes["tx"] != null)
						matrix.OffsetX = double.Parse(m.Attributes["tx"].Value);
					if (m.Attributes["ty"] != null)
						matrix.OffsetY = double.Parse(m.Attributes["ty"].Value);

					//http://math.stackexchange.com/questions/13150/extracting-rotation-scale-values-from-2d-transformation-matrix
					double tX = matrix.OffsetX;
					double tY = matrix.OffsetY;
					double sX = Math.Sign(matrix.M11) * Math.Sqrt(matrix.M11 * matrix.M11 + matrix.M12 * matrix.M12);
					double sY = Math.Sign(matrix.M22) * Math.Sqrt(matrix.M21 * matrix.M21 + matrix.M22 * matrix.M22);
					double theta = Math.Atan2(matrix.M12, matrix.M11);

					double w = Math.Round(sX * desc.Size.Width);
					double h = Math.Round(sY * desc.Size.Height);
					double x =
						Math.Round(tX - ((w * sX / 2 * (1.0 - Math.Cos(theta))) + (h * sY / 2 * Math.Sin(theta))) -
						           desc.OffsetPoint.X * sX);
					double y =
						Math.Round(tY - ((h * sY / 2 * (1.0 - Math.Cos(theta))) - (w * sX / 2 * Math.Sin(theta))) -
						           desc.OffsetPoint.Y * sY);
					double r = Math.Round(theta * 180 / Math.PI);

					t.Width = w;
					t.Height = h;
					Canvas.SetLeft(t, x);
					Canvas.SetTop(t, y);
					t.Rotation = r;
				}

				canvas.Children.Add(t);
			}
		}
	}
}