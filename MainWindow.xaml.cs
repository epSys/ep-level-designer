﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Win32;

namespace EPDesigner
{
	/// <summary>
	///     Interaction logic for Window1.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			TileList.ItemsSource = TileDesc.Tiles.Values;
			TileList.DisplayMemberPath = "Name";
			DependencyPropertyDescriptor.FromProperty(Tile.ActiveTileProperty, typeof(Canvas))
				.AddValueChanged(Canvas, ActiveTileChanged);
		}

		private void ActiveTileChanged(object sender, EventArgs e)
		{
			PropEditor.SelectedObject = Tile.GetActiveTile(Canvas);
			if (PropEditor.SelectedObject != null)
			{
				PropEditor.Properties.Clear();
				if (Tile.GetActiveTile(Canvas).Descriptor.HasName)
					PropEditor.Properties.Add(new PropertyItem(PropEditor, PropEditor.SelectedObject,
						DependencyPropertyDescriptor.FromProperty(Tile.TileNameProperty, typeof(Tile))));
				PropEditor.Properties.Add(new PropertyItem(PropEditor, PropEditor.SelectedObject,
					DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, typeof(Tile))));
				PropEditor.Properties.Add(new PropertyItem(PropEditor, PropEditor.SelectedObject,
					DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, typeof(Tile))));

				if (Tile.GetActiveTile(Canvas).Descriptor.CanRotate)
					PropEditor.Properties.Add(new PropertyItem(PropEditor, PropEditor.SelectedObject,
						DependencyPropertyDescriptor.FromProperty(Tile.RotationProperty, typeof(Tile))));

				if (Tile.GetActiveTile(Canvas).Descriptor.CanResize)
				{
					PropEditor.Properties.Add(new PropertyItem(PropEditor, PropEditor.SelectedObject,
						DependencyPropertyDescriptor.FromProperty(WidthProperty, typeof(Tile))));
					PropEditor.Properties.Add(new PropertyItem(PropEditor, PropEditor.SelectedObject,
						DependencyPropertyDescriptor.FromProperty(HeightProperty, typeof(Tile))));
				}

				if (Tile.GetActiveTile(Canvas).Descriptor.HasTag)
					PropEditor.Properties.Add(new PropertyItem(PropEditor, PropEditor.SelectedObject,
						DependencyPropertyDescriptor.FromProperty(TagProperty, typeof(Tile))));
			}
		}

		public static class MouseUtilities
		{
			public static Point CorrectGetPosition(Visual relativeTo)
			{
				var w32Mouse = new Win32Point();
				GetCursorPos(ref w32Mouse);
				return relativeTo.PointFromScreen(new Point(w32Mouse.X, w32Mouse.Y));
			}

			[StructLayout(LayoutKind.Sequential)]
			internal struct Win32Point
			{
				public Int32 X;
				public Int32 Y;
			};

			[DllImport("user32.dll")]
			[return: MarshalAs(UnmanagedType.Bool)]
			internal static extern bool GetCursorPos(ref Win32Point pt);
		}

		private void Canvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			Tile.SetActiveTile(Canvas, null);

			var desc = TileList.SelectedItem as TileDesc;
			if (desc != null)
			{
				var tile = desc.CreateTile();
				//double x = MouseUtilities.CorrectGetPosition(Canvas).X / 2;
				//double y = MouseUtilities.CorrectGetPosition(Canvas).Y / 2;
				//if (desc.Griding != 0)
				//{
				//    x = ((int)x / (int)desc.Griding) * desc.Griding;
				//    y = ((int)y / (int)desc.Griding) * desc.Griding;
				//}
				Canvas.SetLeft(tile, 0);
				Canvas.SetTop(tile, 0);
				Canvas.Children.Add(tile);
				tile.ApplyTemplate();
				tile.BeginMove();
			}
		}

		private void ListBoxItemDoubleClick(object sender, MouseButtonEventArgs e)
		{
			var desc = (sender as ListBoxItem).DataContext as TileDesc;
			double x = Viewer.HorizontalOffset + Viewer.ViewportWidth / 2;
			double y = Viewer.VerticalOffset + Viewer.ViewportHeight / 2;
			if (desc.Griding != 0)
			{
				x = ((int)x / (int)desc.Griding) * desc.Griding;
				y = ((int)y / (int)desc.Griding) * desc.Griding;
			}
			var tile = desc.CreateTile();
			Canvas.SetLeft(tile, x);
			Canvas.SetTop(tile, y);
			Canvas.Children.Add(tile);
		}

		protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
		{
			base.OnPreviewMouseDown(e);
		}

		protected override void OnPreviewKeyDown(KeyEventArgs e)
		{
			if (e.Key == Key.Delete && Tile.GetActiveTile(Canvas) != null)
			{
				Canvas.Children.Remove(Tile.GetActiveTile(Canvas));
				Tile.SetActiveTile(Canvas, null);
			}
			base.OnPreviewKeyDown(e);
		}

		private void TileList_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			TileList.SelectedItem = null;
		}

		private void Load_Click(object sender, RoutedEventArgs e)
		{
			var ofd = new OpenFileDialog();
			ofd.Filter = "Xml Document (*.xml)|*.xml|All Files (*.*)|*.*";
			if (ofd.ShowDialog().GetValueOrDefault())
			{
				LevelData.Load(Canvas, File.ReadAllText(ofd.FileName));
			}
		}

		private void Save_Click(object sender, RoutedEventArgs e)
		{
			var sfd = new SaveFileDialog();
			sfd.Filter = "Xml Document (*.xml)|*.xml|All Files (*.*)|*.*";
			if (sfd.ShowDialog().GetValueOrDefault())
			{
				File.WriteAllText(sfd.FileName, LevelData.Save(Canvas, Path.GetFileNameWithoutExtension(sfd.FileName)));
			}
		}

		private void Export_Click(object sender, RoutedEventArgs e)
		{
			var cfg = new LvCode.LevelConfig();
			cfg.Name = lvlName.Text;
			if (!int.TryParse(compStr.Text, out cfg.CompressionStrength) || cfg.CompressionStrength < 1)
			{
				compStr.Text = "50";
				cfg.CompressionStrength = 50;
			}

			if (!string.IsNullOrEmpty(pwd.Password))
			{
				cfg.Flags = LvCode.LevelFlags.Protected;
				cfg.Password = pwd.Password;
			}
			cfg.Flags |= (LvCode.LevelFlags)mode.SelectedIndex;
			if (allowSuicide.IsChecked ?? false)
				cfg.Flags |= LvCode.LevelFlags.AllowSuicide;
			if (!(chatSuspend.IsChecked ?? false))
				cfg.Flags |= LvCode.LevelFlags.ActiveChat;

			var code = LvCode.Encode(Canvas, cfg);
			LevelCode.Text = code;
		}

		private void Import_Click(object sender, RoutedEventArgs e)
		{
			LvCode.LevelConfig cfg;
			try
			{
				var msg = LvCode.Decode(Canvas, pwd.Password, out cfg, LevelCode.Text);
				if (msg != null)
					MessageBox.Show(msg);
				else
				{
					mode.SelectedIndex = (int)cfg.Flags & 3;
					allowSuicide.IsChecked = (cfg.Flags & LvCode.LevelFlags.AllowSuicide) != 0;
					chatSuspend.IsChecked = (cfg.Flags & LvCode.LevelFlags.ActiveChat) == 0;
					lvlName.Text = cfg.Name ?? "";
				}
			}
			catch
			{
				MessageBox.Show("Error!");
			}
		}

		private void Convert_Click(object sender, RoutedEventArgs e)
		{
			var ofd = new OpenFileDialog();
			ofd.Filter = "Xml Document (*.xml)|*.xml|All Files (*.*)|*.*";
			if (ofd.ShowDialog().GetValueOrDefault())
			{
				LevelCode.Text = LvCode.Convert(File.ReadAllText(ofd.FileName));
			}
		}

		private void Move_Click(object sender, RoutedEventArgs e)
		{
			var pt = new Point(999999, 999999);
			foreach (var i in Canvas.Children)
			{
				var tile = (Tile)i;
				double x = Canvas.GetLeft(tile);
				if (x < pt.X) pt.X = x;
				double y = Canvas.GetTop(tile);
				if (y < pt.Y) pt.Y = y;
			}
			pt.X -= 25;
			pt.Y -= 25;
			pt.X = pt.X - pt.X % 25;
			pt.Y = pt.Y - pt.Y % 25;
			foreach (var i in Canvas.Children)
			{
				var tile = (Tile)i;
				double x = Canvas.GetLeft(tile);
				x -= pt.X;
				Canvas.SetLeft(tile, x);
				double y = Canvas.GetTop(tile);
				y -= pt.Y;
				Canvas.SetTop(tile, y);
			}
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			if (e.Key == Key.S && e.KeyboardDevice.Modifiers == ModifierKeys.Control)
			{
				Save_Click(null, null);
				e.Handled = true;
			}
			else if (e.Key == Key.O && e.KeyboardDevice.Modifiers == ModifierKeys.Control)
			{
				Load_Click(null, null);
				e.Handled = true;
			}
			base.OnKeyDown(e);
		}

		private void LevelCode_TextChanged(object sender, TextChangedEventArgs e)
		{
			charCount.Content = LevelCode.Text.Length;
		}
	}
}