﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EPDesigner
{
	public class Tile : ContentControl
	{
		static Tile()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(Tile), new FrameworkPropertyMetadata(typeof(Tile)));
			TagProperty.OverrideMetadata(typeof(Tile), new FrameworkPropertyMetadata(null, TagChanged));
			MaxWidthProperty.OverrideMetadata(typeof(Tile), new FrameworkPropertyMetadata(ResizingAdorner.MaxW));
			MaxHeightProperty.OverrideMetadata(typeof(Tile), new FrameworkPropertyMetadata(ResizingAdorner.MaxH));
		}

		public Point OffsetPoint
		{
			get { return (Point)GetValue(OffsetPointProperty); }
			set { SetValue(OffsetPointProperty, value); }
		}

		public static readonly DependencyProperty OffsetPointProperty =
			DependencyProperty.Register("OffsetPoint", typeof(Point), typeof(Tile), new UIPropertyMetadata(new Point(0, 0)));

		public double Rotation
		{
			get { return (double)GetValue(RotationProperty); }
			set { SetValue(RotationProperty, value); }
		}

		public static readonly DependencyProperty RotationProperty =
			DependencyProperty.Register("Rotation", typeof(double), typeof(Tile), new UIPropertyMetadata(0.0, AngleChanged));

		public static Tile GetActiveTile(DependencyObject obj)
		{
			return (Tile)obj.GetValue(ActiveTileProperty);
		}

		public static void SetActiveTile(DependencyObject obj, Tile value)
		{
			obj.SetValue(ActiveTileProperty, value);
		}

		public static readonly DependencyProperty ActiveTileProperty =
			DependencyProperty.RegisterAttached("ActiveTile", typeof(Tile), typeof(Tile),
				new UIPropertyMetadata(null, ActiveTileChanged));

		private static void ActiveTileChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (e.OldValue != null)
				(e.OldValue as Tile).HUGZZ.IsActive = false;
			if (e.NewValue != null)
				(e.NewValue as Tile).HUGZZ.IsActive = true;
		}

		private static void AngleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if ((d as Tile).GetTemplateChild("PART_content") != null)
				((d as Tile).GetTemplateChild("PART_content") as FrameworkElement).RenderTransform = new RotateTransform(
					(double)e.NewValue,
					(d as Tile).Descriptor.Size.Width / 2,
					(d as Tile).Descriptor.Size.Height / 2);
		}

		private static void TagChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if ((d as Tile).GetTemplateChild("PART_content") != null)
				(((d as Tile).GetTemplateChild("PART_content") as ContentPresenter).Content as Canvas).Tag = e.NewValue;
		}

		public TileDesc Descriptor { get; set; }

		public string TileName
		{
			get { return (string)GetValue(TileNameProperty); }
			set { SetValue(TileNameProperty, value); }
		}

		public static readonly DependencyProperty TileNameProperty =
			DependencyProperty.Register("TileName", typeof(string), typeof(Tile), new UIPropertyMetadata(""));


		private Hugger HUGZZ;

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			(HUGZZ = GetTemplateChild("PART_hugger") as Hugger).Tile = this;
			(GetTemplateChild("PART_content") as FrameworkElement).LayoutTransform = new RotateTransform(Rotation,
				Descriptor.Size.Width / 2, Descriptor.Size.Height / 2);
		}

		public void BeginMove()
		{
			HUGZZ.m = true;
			SetActiveTile(Parent, this);
			HUGZZ.pt = new Point(Descriptor.Size.Width / 2, Descriptor.Size.Height / 2);
			HUGZZ.CaptureMouse();
		}
	}
}