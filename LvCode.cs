﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml;
using CompressSharper;

namespace EPDesigner
{
	internal static class LvCode
	{
		private static T trace<T>(T x)
		{
			Debug.WriteLine(x);
			return x;
		}

		[Flags]
		public enum LevelFlags
		{
			Flow_Normal = 0,
			Flow_None = 1,
			Flow_All = 2,

			Protected = 4,
			AllowSuicide = 8,
			ActiveChat = 16,
		}

		public struct LevelConfig
		{
			public string Name;
			public LevelFlags Flags;
			public string Password;
			public int CompressionStrength;
		}

		private static readonly Random rand = new Random();

		public static string Encode(Canvas canvas, LevelConfig cfg)
		{
			var ret = new StringBuilder();
			ret.AppendFormat("{0}~#-", cfg.Name);

			var tiles = canvas.Children.OfType<Tile>().ToArray();
			var ms = new List<Matrix> { Matrix.Identity };
			var mis = new List<int>();
			var mxs = new List<Matrix>();
			foreach (var i in tiles)
			{
				if (i.Width > i.MaxWidth) i.Width = i.MaxWidth;
				if (i.Width < i.MinWidth) i.Width = i.MinWidth;
				if (i.Height > i.MaxHeight) i.Height = i.MaxHeight;
				if (i.Height < i.MinHeight) i.Height = i.MinHeight;
				var matrix = new Matrix();
				var xScale = i.ActualWidth / i.Descriptor.Size.Width;
				var yScale = i.ActualHeight / i.Descriptor.Size.Height;
				matrix.ScaleAt(
					xScale,
					yScale,
					i.ActualWidth * xScale / 2,
					i.ActualWidth * xScale / 2);
				matrix.OffsetX = matrix.OffsetY = 0;
				matrix.RotateAt(i.Rotation,
					i.ActualWidth * xScale / 2,
					i.ActualHeight * yScale / 2);
				mxs.Add(matrix);

				matrix.OffsetX = matrix.OffsetY = 0;
				int idx = ms.IndexOf(matrix);
				if (idx < 0)
				{
					idx = ms.Count;
					ms.Add(matrix);
				}
				mis.Add(idx);
			}

			var str = new MemoryStream();
			var wtr = new BinaryWriter(str);
			wtr.Write(0x564C5045);
			wtr.Write((byte)1);
			wtr.Write((ushort)(rand.Next()));
			wtr.Write((byte)cfg.Flags);
			if ((cfg.Flags & LevelFlags.Protected) != 0)
				wtr.Write(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(cfg.Password)));

			wtr.Write(ms.Count - 1);
			foreach (var m in ms.Skip(1))
			{
				wtr.Write((float)m.M11);
				wtr.Write((float)m.M12);
				wtr.Write((float)m.M21);
				wtr.Write((float)m.M22);
			}
			wtr.Write(tiles.Length);
			int x = 0;
			foreach (var i in tiles)
			{
				char id = i.Descriptor.TileID;
				string name = i.TileName;
				var xScale = i.ActualWidth / i.Descriptor.Size.Width;
				var yScale = i.ActualHeight / i.Descriptor.Size.Height;
				var trX = Canvas.GetLeft(i) + i.Descriptor.OffsetPoint.X * xScale;
				var trY = Canvas.GetTop(i) + i.Descriptor.OffsetPoint.Y * yScale;

				wtr.Write((byte)(TileDesc.DescBits[id] | (byte)(mis[x] != 0 ? 0x80 : 0)));
				if (i.Descriptor.HasTag)
				{
					var tag = i.Tag as string;
					if (string.IsNullOrEmpty(tag)) tag = "";
					wtr.Write((byte)tag.Length);
					wtr.Write(Encoding.UTF8.GetBytes(tag));
				}
				else if (i.Descriptor.HasName)
				{
					wtr.Write((byte)name.Length);
					wtr.Write(Encoding.UTF8.GetBytes(name));
				}
				if (mis[x] != 0)
				{
					wtr.Write(mis[x]);
				}
				var m = mxs[x];
				m.Translate(trX, trY);
				x++;

				wtr.Write((float)Math.Round(m.OffsetX, 2));
				wtr.Write((float)Math.Round(m.OffsetY, 2));
			}
			var buffer = str.ToArray();

			var deflater = new ZopfliDeflater(str);
			deflater.NumberOfIterations = cfg.CompressionStrength;
			deflater.MaximumChainHits = 32768;
			str.SetLength(0);
			deflater.Deflate(buffer, true);

			ret.Append(System.Convert.ToBase64String(str.ToArray()));
			ret.Append("~");
			return ret.ToString();
		}

		public static string Decode(Canvas canvas, string password, out LevelConfig cfg, string dat)
		{
			cfg = default(LevelConfig);
			if (dat[dat.Length - 1] != '~')
			{
				DecodeV0(canvas, dat);
				return null;
			}
			var i = dat.IndexOf("~#-");
			using (var rdr = new BinaryReader(new DeflateStream(new MemoryStream(
				System.Convert.FromBase64String(dat.Substring(i + 3, dat.Length - i - 4))), CompressionMode.Decompress)))
			{
				if (rdr.ReadUInt32() != 0x564C5045) return "Nope!";

				string msg = "Nope!";
				if (rdr.ReadByte() == 1)
					msg = DecodeV1(rdr, canvas, password, out cfg);

				if (msg == null)
					cfg.Name = dat.Substring(0, i);
				return msg;
			}
		}

		public static string DecodeV1(BinaryReader rdr, Canvas canvas, string password, out LevelConfig cfg)
		{
			rdr.ReadUInt16();
			cfg = new LevelConfig();
			cfg.Flags = (LevelFlags)rdr.ReadByte();
			if ((cfg.Flags & LevelFlags.Protected) != 0)
			{
				var hash = rdr.ReadBytes(16);
				if (!hash.SequenceEqual(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(password))))
					return "Bad password!";
			}

			var ms = new List<Matrix> { Matrix.Identity };
			int count = rdr.ReadInt32();
			for (int i = 0; i < count; i++)
			{
				var matrix = new Matrix();
				matrix.M11 = rdr.ReadSingle();
				matrix.M12 = rdr.ReadSingle();
				matrix.M21 = rdr.ReadSingle();
				matrix.M22 = rdr.ReadSingle();
				ms.Add(matrix);
			}
			canvas.Children.Clear();
			count = rdr.ReadInt32();
			for (int i = 0; i < count; i++)
			{
				byte id = rdr.ReadByte();
				TileDesc desc = TileDesc.BinTiles[TileDesc.DescRBits[(byte)(id & 0x7f)]];
				Tile t = desc.CreateTile();
				t.ApplyTemplate();
				if (desc.HasTag)
					t.Tag = Encoding.UTF8.GetString(rdr.ReadBytes(rdr.ReadByte()));
				if (desc.HasName)
				{
					t.TileName = Encoding.UTF8.GetString(rdr.ReadBytes(rdr.ReadByte()));
				}

				Matrix matrix = ms[0];
				if ((id & 0x80) != 0)
					matrix = ms[rdr.ReadInt32()];
				matrix.OffsetX = rdr.ReadSingle();
				matrix.OffsetY = rdr.ReadSingle();

				//http://math.stackexchange.com/questions/13150/extracting-rotation-scale-values-from-2d-transformation-matrix

				double tX = matrix.OffsetX;
				double tY = matrix.OffsetY;
				double sX = Math.Sqrt(matrix.M11 * matrix.M11 + matrix.M12 * matrix.M12);
				double sY = Math.Sqrt(matrix.M21 * matrix.M21 + matrix.M22 * matrix.M22);
				double theta = Math.Atan2(matrix.M12, matrix.M11);

				double w = Math.Round(sX * desc.Size.Width);
				double h = Math.Round(sY * desc.Size.Height);
				double x =
					Math.Round(tX - ((w * sX / 2 * (1.0 - Math.Cos(theta))) + (h * sY / 2 * Math.Sin(theta))) - desc.OffsetPoint.X * sX);
				double y =
					Math.Round(tY - ((h * sY / 2 * (1.0 - Math.Cos(theta))) - (w * sX / 2 * Math.Sin(theta))) - desc.OffsetPoint.Y * sY);
				double r = Math.Round(theta * 180 / Math.PI);

				t.Width = w;
				t.Height = h;
				Canvas.SetLeft(t, x);
				Canvas.SetTop(t, y);
				t.Rotation = r;
				canvas.Children.Add(t);
			}
			return null;
		}

		public static void DecodeV0(Canvas canvas, string dat)
		{
			canvas.Children.Clear();
			using (
				var rdr =
					new BinaryReader(new DeflateStream(new MemoryStream(System.Convert.FromBase64String(dat.Split('#')[1])),
						CompressionMode.Decompress)))
			{
				int count = rdr.ReadInt32();
				for (int i = 0; i < count; i++)
				{
					TileDesc desc = TileDesc.BinTiles[(char)rdr.ReadByte()];
					Tile t = desc.CreateTile();
					t.ApplyTemplate();
					if (desc.HasTag)
						t.Tag = new string(rdr.ReadChars(rdr.ReadByte()));
					t.TileName = new string(rdr.ReadChars(rdr.ReadByte()));

					var matrix = new Matrix();
					matrix.M11 = rdr.ReadSingle();
					matrix.M12 = rdr.ReadSingle();
					matrix.M21 = rdr.ReadSingle();
					matrix.M22 = rdr.ReadSingle();
					matrix.OffsetX = rdr.ReadSingle();
					matrix.OffsetY = rdr.ReadSingle();

					double tX = matrix.OffsetX;
					double tY = matrix.OffsetY;
					double sX = Math.Sqrt(matrix.M11 * matrix.M11 + matrix.M12 * matrix.M12);
					double sY = Math.Sqrt(matrix.M21 * matrix.M21 + matrix.M22 * matrix.M22);
					double theta = Math.Atan2(matrix.M12, matrix.M11);

					double w = Math.Round(sX * desc.Size.Width);
					double h = Math.Round(sY * desc.Size.Height);
					double x =
						Math.Round(tX - ((w * sX / 2 * (1.0 - Math.Cos(theta))) + (h * sY / 2 * Math.Sin(theta))) -
						           desc.OffsetPoint.X * sX);
					double y =
						Math.Round(tY - ((h * sY / 2 * (1.0 - Math.Cos(theta))) - (w * sX / 2 * Math.Sin(theta))) -
						           desc.OffsetPoint.Y * sY);
					double r = Math.Round(theta * 180 / Math.PI);

					t.Width = w;
					t.Height = h;
					Canvas.SetLeft(t, x);
					Canvas.SetTop(t, y);
					t.Rotation = r;
					canvas.Children.Add(t);
				}
			}
		}

		public static string Convert(string xml)
		{
			var xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml);
			var mgr = new XmlNamespaceManager(xmlDoc.NameTable);
			mgr.AddNamespace("x", "http://ns.adobe.com/xfl/2008/");

			var ret = new StringBuilder();
			ret.Append("Name#");
			using (var str = new MemoryStream())
			{
				var nodes =
					xmlDoc.SelectNodes(
						"/x:DOMSymbolItem/x:timeline/x:DOMTimeline/x:layers/x:DOMLayer/x:frames/x:DOMFrame/x:elements/x:DOMSymbolInstance",
						mgr);

				using (var wtr = new BinaryWriter(new DeflateStream(str, CompressionMode.Compress)))
				{
					wtr.Write(nodes.Count);
					foreach (XmlNode i in nodes)
					{
						string id = i.Attributes["libraryItemName"].Value;
						TileDesc desc;
						if (!TileDesc.XmlTiles.TryGetValue(id, out desc)) continue;
						string name = i.Attributes["name"].Value;

						XmlNode m = i.SelectSingleNode("x:matrix/x:Matrix", mgr);
						Matrix matrix = Matrix.Identity;
						if (m != null)
						{
							if (m.Attributes["a"] != null)
								matrix.M11 = double.Parse(m.Attributes["a"].Value);
							if (m.Attributes["b"] != null)
								matrix.M12 = double.Parse(m.Attributes["b"].Value);
							if (m.Attributes["c"] != null)
								matrix.M21 = double.Parse(m.Attributes["c"].Value);
							if (m.Attributes["d"] != null)
								matrix.M22 = double.Parse(m.Attributes["d"].Value);
							if (m.Attributes["tx"] != null)
								matrix.OffsetX = double.Parse(m.Attributes["tx"].Value);
							if (m.Attributes["ty"] != null)
								matrix.OffsetY = double.Parse(m.Attributes["ty"].Value);
						}
						wtr.Write((byte)desc.TileID);
						if (desc.HasTag)
						{
							wtr.Write((byte)0);
						}
						wtr.Write((byte)name.Length);
						wtr.Write(name.ToCharArray(), 0, name.Length);
						wtr.Write((float)Math.Round(matrix.M11, 2));
						wtr.Write((float)Math.Round(matrix.M12, 2));
						wtr.Write((float)Math.Round(matrix.M21, 2));
						wtr.Write((float)Math.Round(matrix.M22, 2));
						wtr.Write((float)Math.Round(matrix.OffsetX, 2));
						wtr.Write((float)Math.Round(matrix.OffsetY, 2));
					}
				}
				ret.Append(System.Convert.ToBase64String(str.ToArray()));
				return ret.ToString();
			}
		}
	}
}