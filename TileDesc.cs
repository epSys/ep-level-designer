﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace EPDesigner
{
	public class TileDesc
	{
		public string Name { get; set; }
		public string CanvasID { get; set; }
		public string XmlID { get; set; }
		public char TileID { get; set; }
		public Point OffsetPoint { get; set; }
		public Size Size { get; set; }
		public bool CanRotate { get; set; }
		public bool CanResize { get; set; }
		public double Griding { get; set; }
		public bool HasTag { get; set; }
		public bool HasName { get; set; }

		public Tile CreateTile()
		{
			var ret = new Tile
			{
				Descriptor = this,
				OffsetPoint = OffsetPoint,
				Width = Size.Width,
				Height = Size.Height
			};
			ret.ApplyTemplate();
			ret.Content = Application.Current.FindResource(CanvasID);
			return ret;
		}

		public static readonly Dictionary<string, TileDesc> Tiles = new Dictionary<string, TileDesc>
		{
			{
				"end", new TileDesc
				{
					Name = "End Point",
					CanvasID = "end",
					XmlID = "Tiles/finish",
					TileID = 'e',
					CanResize = false,
					CanRotate = false,
					HasTag = false,
					HasName = false,
					Griding = 25,
					OffsetPoint = new Point(0, 0),
					Size = new Size(25, 25)
				}
			},
			{
				"start", new TileDesc
				{
					Name = "Start Point",
					CanvasID = "start",
					XmlID = "Tiles/start",
					TileID = 's',
					CanResize = false,
					CanRotate = false,
					HasTag = false,
					HasName = false,
					Griding = 25,
					OffsetPoint = new Point(0, 0),
					Size = new Size(25, 25)
				}
			},
			{
				"tile", new TileDesc
				{
					Name = "Block",
					CanvasID = "tile",
					XmlID = "Tiles/block",
					TileID = 'b',
					CanResize = false,
					CanRotate = false,
					HasTag = false,
					HasName = false,
					Griding = 25,
					OffsetPoint = new Point(0, 0),
					Size = new Size(25, 25)
				}
			},
			{
				"hiddenTile", new TileDesc
				{
					Name = "Hidden Block",
					CanvasID = "hiddenTile",
					XmlID = "Tiles/hiddenBlock",
					TileID = ' ',
					CanResize = false,
					CanRotate = false,
					HasTag = false,
					HasName = false,
					Griding = 25,
					OffsetPoint = new Point(0, 0),
					Size = new Size(25, 25)
				}
			},
			{
				"popSpike", new TileDesc
				{
					Name = "Pop Spike",
					CanvasID = "popSpike",
					XmlID = "Tiles/popSpike",
					TileID = 'p',
					CanResize = true,
					CanRotate = true,
					HasTag = false,
					HasName = true,
					Griding = 0,
					OffsetPoint = new Point(2, 0),
					Size = new Size(24, 54)
				}
			},
			{
				"spike", new TileDesc
				{
					Name = "Spike",
					CanvasID = "spike",
					XmlID = "Tiles/spike",
					TileID = 'k',
					CanResize = false,
					CanRotate = true,
					HasTag = false,
					HasName = false,
					Griding = 25,
					OffsetPoint = new Point(0, 0),
					Size = new Size(25, 25)
				}
			},
			{
				"halfBlock", new TileDesc
				{
					Name = "Half Block",
					CanvasID = "halfBlock",
					XmlID = "Tiles/halfBlock",
					TileID = 'h',
					CanResize = false,
					CanRotate = false,
					HasTag = false,
					HasName = false,
					Griding = 12.5,
					OffsetPoint = new Point(0, 0),
					Size = new Size(25, 12)
				}
			},
			{
				"grinder", new TileDesc
				{
					Name = "Grinder",
					CanvasID = "grinder",
					XmlID = "Tiles/grinder",
					TileID = 'g',
					CanResize = true,
					CanRotate = false,
					HasTag = false,
					HasName = true,
					Griding = 0,
					OffsetPoint = new Point(78.075, 78.075),
					Size = new Size(156.15, 156.15)
					//OffsetPoint = new Point(78.075, 78.075),
					//Size = new Size(156.15, 156.15)
				}
			},
			{
				"axe", new TileDesc
				{
					Name = "Axe",
					CanvasID = "axe",
					XmlID = "Tiles/axe",
					TileID = 'a',
					CanResize = true,
					CanRotate = true,
					HasTag = false,
					HasName = true,
					Griding = 0,
					OffsetPoint = new Point(-57.5, 0),
					Size = new Size(115, 63.3)
				}
			},
			{
				"rightTreadmill", new TileDesc
				{
					Name = "Right Treadmill",
					CanvasID = "rightTreadmill",
					XmlID = "Tiles/rTrendmill",
					TileID = '>',
					CanResize = false,
					CanRotate = false,
					HasTag = false,
					HasName = false,
					Griding = 25,
					OffsetPoint = new Point(0, 0),
					Size = new Size(25, 25)
				}
			},
			{
				"leftTreadmill", new TileDesc
				{
					Name = "Left Treadmill",
					CanvasID = "leftTreadmill",
					XmlID = "Tiles/lTrendmill",
					TileID = '<',
					CanResize = false,
					CanRotate = false,
					HasTag = false,
					HasName = false,
					Griding = 25,
					OffsetPoint = new Point(0, 0),
					Size = new Size(25, 25)
				}
			},
			{
				"bouncer", new TileDesc
				{
					Name = "Bouncer",
					CanvasID = "bouncer",
					XmlID = "Tiles/bouncer",
					TileID = 'x',
					CanResize = false,
					CanRotate = true,
					HasTag = false,
					HasName = true,
					Griding = 25,
					OffsetPoint = new Point(0, 0),
					Size = new Size(25, 25)
				}
			},
			{
				"fallSpike", new TileDesc
				{
					Name = "Falling Spike",
					CanvasID = "fallSpike",
					XmlID = "Tiles/fallSpike",
					TileID = 'f',
					CanResize = false,
					CanRotate = true,
					HasTag = false,
					HasName = false,
					Griding = 25,
					OffsetPoint = new Point(2, 0),
					Size = new Size(23.25, 25)
				}
			},
			{
				"laser", new TileDesc
				{
					Name = "Laser Cannon",
					CanvasID = "laser",
					XmlID = "Tiles/laser",
					TileID = 'l',
					CanResize = false,
					CanRotate = true,
					HasTag = false,
					HasName = false,
					Griding = 0,
					OffsetPoint = new Point(10, 10),
					Size = new Size(20, 20)
				}
			},
			{
				"teleport", new TileDesc
				{
					Name = "Teleporter",
					CanvasID = "teleport",
					XmlID = "Tiles/teleporter",
					TileID = 't',
					CanResize = true,
					CanRotate = false,
					HasTag = false,
					HasName = true,
					Griding = 0,
					OffsetPoint = new Point(12, 37.05),
					Size = new Size(25.05, 42.05)
				}
			},
			{
				"checkPoint", new TileDesc
				{
					Name = "Checkpoint",
					CanvasID = "checkPoint",
					XmlID = "Tiles/checkPoint",
					TileID = 'c',
					CanResize = false,
					CanRotate = false,
					HasTag = false,
					HasName = false,
					Griding = 0,
					OffsetPoint = new Point(1.75, 39),
					Size = new Size(3.5, 39)
				}
			},
			{
				"textBlock", new TileDesc
				{
					Name = "Text Block",
					CanvasID = "textBlock",
					XmlID = "Tiles/textBlock",
					TileID = 'q',
					CanResize = true,
					CanRotate = true,
					HasTag = true,
					HasName = false,
					Griding = 0,
					OffsetPoint = new Point(0, 0),
					Size = new Size(50, 50)
				}
			},
		};

		public static readonly Dictionary<string, TileDesc> XmlTiles = new Dictionary<string, TileDesc>
		{
			{ "Tiles/finish", Tiles["end"] },
			{ "Tiles/start", Tiles["start"] },
			{ "Tiles/block", Tiles["tile"] },
			{ "Tiles/hiddenBlock", Tiles["hiddenTile"] },
			{ "Tiles/popSpike", Tiles["popSpike"] },
			{ "Tiles/spike", Tiles["spike"] },
			{ "Tiles/halfBlock", Tiles["halfBlock"] },
			{ "Tiles/grinder", Tiles["grinder"] },
			{ "Tiles/axe", Tiles["axe"] },
			{ "Tiles/rTrendmill", Tiles["rightTreadmill"] },
			{ "Tiles/lTrendmill", Tiles["leftTreadmill"] },
			{ "Tiles/bouncer", Tiles["bouncer"] },
			{ "Tiles/fallSpike", Tiles["fallSpike"] },
			{ "Tiles/laser", Tiles["laser"] },
			{ "Tiles/teleporter", Tiles["teleport"] },
			{ "Tiles/checkPoint", Tiles["checkPoint"] },
			{ "Tiles/textBlock", Tiles["textBlock"] },
		};

		public static readonly Dictionary<char, byte> DescBits = new Dictionary<char, byte>
		{
			{ Tiles["end"].TileID, 1 },
			{ Tiles["start"].TileID, 2 },
			{ Tiles["tile"].TileID, 3 },
			{ Tiles["hiddenTile"].TileID, 4 },
			{ Tiles["popSpike"].TileID, 5 },
			{ Tiles["spike"].TileID, 6 },
			{ Tiles["halfBlock"].TileID, 7 },
			{ Tiles["grinder"].TileID, 8 },
			{ Tiles["axe"].TileID, 9 },
			{ Tiles["rightTreadmill"].TileID, 10 },
			{ Tiles["leftTreadmill"].TileID, 11 },
			{ Tiles["bouncer"].TileID, 12 },
			{ Tiles["fallSpike"].TileID, 13 },
			{ Tiles["laser"].TileID, 14 },
			{ Tiles["teleport"].TileID, 15 },
			{ Tiles["checkPoint"].TileID, 16 },
			{ Tiles["textBlock"].TileID, 17 },
		};

		public static readonly Dictionary<byte, char> DescRBits = new Dictionary<byte, char>
		{
			{ 1, Tiles["end"].TileID },
			{ 2, Tiles["start"].TileID },
			{ 3, Tiles["tile"].TileID },
			{ 4, Tiles["hiddenTile"].TileID },
			{ 5, Tiles["popSpike"].TileID },
			{ 6, Tiles["spike"].TileID },
			{ 7, Tiles["halfBlock"].TileID },
			{ 8, Tiles["grinder"].TileID },
			{ 9, Tiles["axe"].TileID },
			{ 10, Tiles["rightTreadmill"].TileID },
			{ 11, Tiles["leftTreadmill"].TileID },
			{ 12, Tiles["bouncer"].TileID },
			{ 13, Tiles["fallSpike"].TileID },
			{ 14, Tiles["laser"].TileID },
			{ 15, Tiles["teleport"].TileID },
			{ 16, Tiles["checkPoint"].TileID },
			{ 17, Tiles["textBlock"].TileID },
		};

		public static readonly Dictionary<char, TileDesc> BinTiles = new Dictionary<char, TileDesc>
		{
			{ Tiles["end"].TileID, Tiles["end"] },
			{ Tiles["start"].TileID, Tiles["start"] },
			{ Tiles["tile"].TileID, Tiles["tile"] },
			{ Tiles["hiddenTile"].TileID, Tiles["hiddenTile"] },
			{ Tiles["popSpike"].TileID, Tiles["popSpike"] },
			{ Tiles["spike"].TileID, Tiles["spike"] },
			{ Tiles["halfBlock"].TileID, Tiles["halfBlock"] },
			{ Tiles["grinder"].TileID, Tiles["grinder"] },
			{ Tiles["axe"].TileID, Tiles["axe"] },
			{ Tiles["rightTreadmill"].TileID, Tiles["rightTreadmill"] },
			{ Tiles["leftTreadmill"].TileID, Tiles["leftTreadmill"] },
			{ Tiles["bouncer"].TileID, Tiles["bouncer"] },
			{ Tiles["fallSpike"].TileID, Tiles["fallSpike"] },
			{ Tiles["laser"].TileID, Tiles["laser"] },
			{ Tiles["teleport"].TileID, Tiles["teleport"] },
			{ Tiles["checkPoint"].TileID, Tiles["checkPoint"] },
			{ Tiles["textBlock"].TileID, Tiles["textBlock"] },
		};
	}
}