﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace EPDesigner
{
	public class Hugger : ContentControl
	{
		public Tile Tile
		{
			get { return (Tile)GetValue(TileProperty); }
			set { SetValue(TileProperty, value); }
		}

		public static readonly DependencyProperty TileProperty =
			DependencyProperty.Register("Tile", typeof(Tile), typeof(Hugger), new UIPropertyMetadata(null));

		public bool IsActive
		{
			get { return (bool)GetValue(IsActiveProperty); }
			set { SetValue(IsActiveProperty, value); }
		}

		public static readonly DependencyProperty IsActiveProperty =
			DependencyProperty.Register("IsActive", typeof(bool), typeof(Hugger),
				new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, IsActiveChanged));

		private static void IsActiveChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var hugger = (Hugger)sender;
			var layer = AdornerLayer.GetAdornerLayer(hugger.Tile);
			if (layer == null || !hugger.Tile.Descriptor.CanResize) return;
			if ((bool)e.NewValue)
				layer.Add(new ResizingAdorner(hugger.Tile));
			else
				layer.Remove(layer.GetAdorners(hugger.Tile)[0]);
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			base.OnRender(drawingContext);
			if (IsActive)
			{
				drawingContext.PushGuidelineSet(new GuidelineSet(new[] { 0.5 }, new[] { 0.5 }));
				drawingContext.DrawRectangle(Brushes.Transparent, new Pen(Brushes.Black, 1),
					new Rect(3, 3, ActualWidth - 6, ActualHeight - 6));
				if (Tile.Descriptor.CanResize)
				{
					drawingContext.DrawRectangle(Brushes.White, new Pen(Brushes.Black, 1), new Rect(-1, -1, 8, 8));
					drawingContext.DrawRectangle(Brushes.White, new Pen(Brushes.Black, 1), new Rect(ActualWidth - 7, -1, 8, 8));
					drawingContext.DrawRectangle(Brushes.White, new Pen(Brushes.Black, 1), new Rect(-1, ActualHeight - 7, 8, 8));
					drawingContext.DrawRectangle(Brushes.White, new Pen(Brushes.Black, 1),
						new Rect(ActualWidth - 7, ActualHeight - 7, 8, 8));
				}
				drawingContext.Pop();
			}
		}

		protected override HitTestResult HitTestCore(PointHitTestParameters hitTestParameters)
		{
			if (Tile.InputHitTest(hitTestParameters.HitPoint) != null)
				return new PointHitTestResult(this, hitTestParameters.HitPoint);
			return null;
		}

		internal bool m = false;
		internal Point pt;

		protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
		{
			base.OnPreviewMouseDown(e);

			if (e.LeftButton == MouseButtonState.Pressed)
			{
				Tile.SetActiveTile(Tile.Parent, Tile);
				e.Handled = true;
			}
			if (e.LeftButton == MouseButtonState.Pressed && IsActive)
			{
				pt = e.GetPosition(Tile);
				m = true;
				CaptureMouse();
			}
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);

			if (IsActive && m)
			{
				var nPt = e.GetPosition(Tile);
				Canvas.SetLeft(Tile, Canvas.GetLeft(Tile) + nPt.X - pt.X);
				Canvas.SetTop(Tile, Canvas.GetTop(Tile) + nPt.Y - pt.Y);
			}
		}

		protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonUp(e);

			if (IsActive && m)
			{
				m = false;
				ReleaseMouseCapture();

				if (Tile.Descriptor.Griding != 0)
				{
					double x = Canvas.GetLeft(Tile);
					double y = Canvas.GetTop(Tile);

					double reX = x % Tile.Descriptor.Griding;
					if (reX > Tile.Descriptor.Griding / 2)
						x += Tile.Descriptor.Griding - reX;
					else
						x -= reX;
					double reY = y % Tile.Descriptor.Griding;
					if (reY > Tile.Descriptor.Griding / 2)
						y += Tile.Descriptor.Griding - reY;
					else
						y -= reY;

					Canvas.SetLeft(Tile, x);
					Canvas.SetTop(Tile, y);
				}
			}
		}
	}
}